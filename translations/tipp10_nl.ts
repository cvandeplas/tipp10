<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>AbcRainWidget</name>
    <message>
        <source>Press space bar to start</source>
        <translation>Druk op de spatiebalk om te starten</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Pauze</translation>
    </message>
    <message>
        <source>E&amp;xit Game</source>
        <translation>Spel &amp;verlaten</translation>
    </message>
    <message>
        <source>Number of points:</source>
        <translation>Aantal punten:</translation>
    </message>
    <message>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
    <message>
        <source>Points:</source>
        <translation>Punten:</translation>
    </message>
    <message>
        <source>Press space bar to proceed</source>
        <translation>Druk op de spatiebalk om verder te gaan</translation>
    </message>
</context>
<context>
    <name>CharSqlModel</name>
    <message>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
</context>
<context>
    <name>CharTableSql</name>
    <message>
        <source>Characters</source>
        <translation>Tekens</translation>
    </message>
    <message>
        <source>Target Errors</source>
        <translation>Doelfouten</translation>
    </message>
    <message>
        <source>Actual Errors</source>
        <translation>Werkelijke fouten</translation>
    </message>
    <message>
        <source>Frequency</source>
        <translation>Frequentie</translation>
    </message>
    <message>
        <source>Error Rate</source>
        <translation>Foutpercentage</translation>
    </message>
    <message>
        <source>This column shows all of the
characters typed</source>
        <translation>Deze kolom toont alle
getypte tekens</translation>
    </message>
    <message>
        <source>The character was supposed to be typed, but wasn&apos;t</source>
        <translation>Het karakter had moeten worden getypt, maar was niet</translation>
    </message>
    <message>
        <source>Character was mistyped</source>
        <translation>Karakter is verkeerd getypt</translation>
    </message>
    <message>
        <source>This column indicates the total frequency of each
character shown</source>
        <translation>Deze kolom geeft de totale frequentie aan van elk getoond teken</translation>
    </message>
    <message>
        <source>The error rate shows which characters give
you the most problems. The error rate is
calculated from the value &quot;Target Error&quot;
and the value &quot;Frequency&quot;.</source>
        <translation>Het foutenpercentage laat zien welke tekens
je de meeste problemen geven. Het foutenpercentage wordt
berekend uit de waarde &quot;Doelfout&quot;
en de waarde &quot;Frequentie&quot;.</translation>
    </message>
    <message>
        <source>Reset characters</source>
        <translation>Tekens resetten</translation>
    </message>
    <message>
        <source>Recorded error rates affect the intelligence feature and the selection of the text to be dictated. If the error rate for a certain character is excessively high it might be useful to reset the list.

All recorded characters will now be deleted.

Do you still wish to proceed?
</source>
        <translation>Opgenomen foutenpercentages zijn van invloed op de intelligentiefunctie en de selectie van de te dicteren tekst. Als het foutenpercentage voor een bepaald teken te hoog is, kan het nuttig zijn om de lijst te resetten.

Alle opgenomen tekens worden nu worden verwijderd.

Wilt u toch doorgaan?
</translation>
    </message>
</context>
<context>
    <name>ErrorMessage</name>
    <message>
        <source>The process will be aborted.</source>
        <translation>Het proces wordt afgebroken.</translation>
    </message>
    <message>
        <source>The update will be aborted.</source>
        <translation>De update wordt afgebroken.</translation>
    </message>
    <message>
        <source>The program will be aborted.</source>
        <translation>Het programma wordt afgebroken.</translation>
    </message>
    <message>
        <source>Cannot load the program logo.</source>
        <translation>Kan het programmalogo niet laden.</translation>
    </message>
    <message>
        <source>Cannot load the keyboard bitmap.</source>
        <translation>Kan de bitmap van het toetsenbord niet laden.</translation>
    </message>
    <message>
        <source>Cannot load the timer background.</source>
        <translation>Kan de timerachtergrond niet laden.</translation>
    </message>
    <message>
        <source>Cannot load the status bar background.</source>
        <translation>Kan de achtergrond van de statusbalk niet laden.</translation>
    </message>
    <message>
        <source>Cannot find the database %1. The file could not be imported.
Please check whether it is a readable text file.</source>
        <translation>Kan de database %1 niet vinden. Het bestand kon niet worden geïmporteerd.
Controleer of het een leesbaar tekstbestand is.</translation>
    </message>
    <message>
        <source>Cannot find the database in the specified directory.
TIPP10 is trying to create a new, empty database in the specified directory.

You can change the path to the database in the program settings.
</source>
        <translation>Kan de database niet vinden in de opgegeven map.
TIPP10 probeert een nieuwe, lege database aan te maken in de opgegeven map.

U kunt het pad naar de database wijzigen in de programma-instellingen.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in your HOME directory. There may be no permission to write.
TIPP10 is trying to use the original database in the program directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Kan de gebruikersdatabase in uw HOME-directory niet maken. Mogelijk is er geen toestemming om te schrijven.
TIPP10 probeert de oorspronkelijke database in de programmadirectory te gebruiken.

U kunt het pad naar de database in de programma instellingen later.
</translation>
    </message>
    <message>
        <source>Cannot create the user database in the specified directory. There may be no directory or no permission to write.
TIPP10 is trying to create a new, empty database in your HOME directory.

You can change the path to the database in the program settings later.
</source>
        <translation>Kan de gebruikersdatabase niet maken in de opgegeven map. Er is mogelijk geen map of geen toestemming om te schrijven.
TIPP10 probeert een nieuwe, lege database in uw HOME-map te maken.

U kunt het pad wijzigen later naar de database in de programma-instellingen.
</translation>
    </message>
    <message>
        <source>Connection to the database failed.</source>
        <translation>Verbinding met de database mislukt.</translation>
    </message>
    <message>
        <source>The user table with lesson data  cannot be emptied.
SQL statement failed.</source>
        <translation>De gebruikerstabel met lesgegevens kan niet worden geleegd.
SQL-instructie is mislukt.</translation>
    </message>
    <message>
        <source>The user table with error data cannot be emptied.
SQL statement failed.</source>
        <translation>De gebruikerstabel met foutgegevens kan niet worden geleegd.
SQL-instructie is mislukt.</translation>
    </message>
    <message>
        <source>No lessons exist.</source>
        <translation>Er bestaan geen lessen.</translation>
    </message>
    <message>
        <source>No lesson selected.
Please select a lesson.</source>
        <translation>Geen les geselecteerd.
Selecteer een les.</translation>
    </message>
    <message>
        <source>Cannot create the lesson.
SQL statement failed.</source>
        <translation>Kan de les niet maken.
SQL-instructie mislukt.</translation>
    </message>
    <message>
        <source>The lesson could not be updated because there is no
access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>De les kon niet worden bijgewerkt omdat er geen
toegang tot de database is.

Als dit probleem zich pas voordeed nadat de software enige tijd
soepel draaide, is de database naar verwachting
beschadigd (bijv. crashen van de computer).
Om te controleren of de database al dan niet beschadigd is,
kunt u het databasebestand hernoemen en de software herstarten (er wordt automatisch een nieuwe, lege database aangemaakt).
U kunt de database vinden pad &quot;%1&quot; in de Algemene instellingen.

Als dit probleem optrad nadat de software voor het eerst
opstartte, controleer dan de schrijfrechten op het database
bestand.</translation>
    </message>
    <message>
        <source>The user typing errors could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>De typefouten van de gebruiker konden niet worden bijgewerkt omdat
er geen toegang tot de database is.

Als dit probleem zich pas voordeed nadat de software enige tijd
soepel had gewerkt, wordt verwacht dat de database is beschadigd (bijv. crashen van de computer).
Om te controleren of de database al dan niet beschadigd is,
kunt u het databasebestand hernoemen en de software opnieuw starten (er wordt automatisch een nieuwe, lege database aangemaakt).
U kunt vinden het databasepad &quot;%1&quot; in de Algemene instellingen.

Als dit probleem zich voordeed nadat de software voor het eerst
opstartte, controleer dan de schrijfrechten op het database
bestand.</translation>
    </message>
    <message>
        <source>The user lesson data could not be updated because
there is no access to the database.

If this problem only occurred after the software had been
running smoothly for some time, the database is expected
to have been damaged (eg crashing of the computer).
To check whether or not the database has been damaged,
you can rename the database file and restart the software (it
will create a new, empty database automatically).
You can find the database path &quot;%1&quot; in the General Settings.

If this problem occurred after the first time the software was
started, please check the write privileges on the database
file.</source>
        <translation>De gebruikerslesgegevens konden niet worden bijgewerkt omdat
er geen toegang tot de database is.

Als dit probleem zich pas voordeed nadat de software enige tijd
soepel had gewerkt, wordt verwacht dat de database is beschadigd (bijv. crashen van de computer).
Om te controleren of de database al dan niet beschadigd is,
kunt u het databasebestand hernoemen en de software opnieuw starten (er wordt automatisch een nieuwe, lege database aangemaakt).
U kunt vinden het databasepad &quot;%1&quot; in de Algemene instellingen.

Als dit probleem zich voordeed nadat de software voor het eerst
opstartte, controleer dan de schrijfrechten op het database
bestand.</translation>
    </message>
    <message>
        <source>Cannot save the lesson.
SQL statement failed.</source>
        <translation>Kan de les niet opslaan.
SQL-instructie is mislukt.</translation>
    </message>
    <message>
        <source>Cannot retrieve the lesson.
SQL statement failed.</source>
        <translation>Kan de les niet ophalen.
SQL-instructie mislukt.</translation>
    </message>
    <message>
        <source>Cannot analyze the lesson.
SQL statement failed.</source>
        <translation>Kan de les niet analyseren.
SQL-instructie mislukt.</translation>
    </message>
    <message>
        <source>The file could not be imported.
Please check whether it is a readable text file.
</source>
        <translation>Het bestand kon niet worden geïmporteerd.
Controleer of het een leesbaar tekstbestand is.
</translation>
    </message>
    <message>
        <source>The file could not be imported because it is empty.
Please check whether it is a readable text file with content.
</source>
        <translation>Het bestand kon niet worden geïmporteerd omdat het leeg is.
Controleer of het een leesbaar tekstbestand met inhoud is.
</translation>
    </message>
    <message>
        <source>The file could not be imported.
Please check the spelling of the web address;
it must be a valid URL and a readable text file.
Please also check your internet connection.</source>
        <translation>Het bestand kon niet worden geïmporteerd.
Controleer de spelling van het webadres;
dit moet een geldige URL en een leesbaar tekstbestand zijn.
Controleer ook uw internetverbinding.</translation>
    </message>
    <message>
        <source>The file could not be exported.
Please check to see whether it is a writable text file.
</source>
        <translation>Het bestand kon niet worden geëxporteerd.
Controleer of het een beschrijfbaar tekstbestand is.
</translation>
    </message>
    <message>
        <source>Cannot create temporary file.</source>
        <translation>Kan tijdelijk bestand niet maken.</translation>
    </message>
    <message>
        <source>Cannot execute the update process.
Please check your internet connection and proxy settings.</source>
        <translation>Kan het updateproces niet uitvoeren.
Controleer uw internetverbinding en proxy-instellingen.</translation>
    </message>
    <message>
        <source>Cannot read the online update version.</source>
        <translation>Kan de online updateversie niet lezen.</translation>
    </message>
    <message>
        <source>Cannot read the database update version.</source>
        <translation>Kan de updateversie van de database niet lezen.</translation>
    </message>
    <message>
        <source>Cannot execute the SQL statement.</source>
        <translation>Kan de SQL-instructie niet uitvoeren.</translation>
    </message>
    <message>
        <source>Cannot find typing mistake definitions.</source>
        <translation>Kan typefoutdefinities niet vinden.</translation>
    </message>
    <message>
        <source>Cannot create temporary file.
Update failed.</source>
        <translation>Kan tijdelijk bestand niet maken.
Update mislukt.</translation>
    </message>
    <message>
        <source>Cannot create analysis table.</source>
        <translation>Kan analysetabel niet maken.</translation>
    </message>
    <message>
        <source>Cannot create analysis index.</source>
        <translation>Kan geen analyse-index maken.</translation>
    </message>
    <message>
        <source>Cannot fill analysis table with values.</source>
        <translation>Kan de analysetabel niet vullen met waarden.</translation>
    </message>
    <message>
        <source>An error has occured.</source>
        <translation>Er is een fout opgetreden.</translation>
    </message>
    <message>
        <source>
(Error number: %1)
</source>
        <translation>
(Foutnummer: %1)
</translation>
    </message>
</context>
<context>
    <name>EvaluationWidget</name>
    <message>
        <source>Report</source>
        <translation>Rapporteren</translation>
    </message>
    <message>
        <source>Overview of Lessons</source>
        <translation>Overzicht van lessen</translation>
    </message>
    <message>
        <source>Progress of Lessons</source>
        <translation>Voortgang van lessen</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Tekens</translation>
    </message>
    <message>
        <source>Fingers</source>
        <translation>Vingers</translation>
    </message>
    <message>
        <source>Comparison Table</source>
        <translation>Vergelijkingstabel</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Sluiten</translation>
    </message>
    <message>
        <source>Use examples</source>
        <translation>Gebruik voorbeelden</translation>
    </message>
    <message>
        <source>Please note that you get better scores for slow typing without errors, than for fast typing with lots of errors!</source>
        <translation>Let op: u krijgt betere scores voor langzaam typen zonder fouten, dan voor snel typen met veel fouten!</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <source>For example, this equates to …</source>
        <translation>Dit komt bijvoorbeeld overeen met …</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Prestaties</translation>
    </message>
    <message>
        <source>Points</source>
        <translation>Punten</translation>
    </message>
    <message>
        <source>%1 cpm and %2 errors in %3 minutes</source>
        <translation>%1 tpm en %2 fouten in %3 minuten</translation>
    </message>
    <message>
        <source>No experience in touch typing</source>
        <translation>Geen ervaring met blind typen</translation>
    </message>
    <message>
        <source>First steps in touch typing</source>
        <translation>Eerste stappen in blind typen</translation>
    </message>
    <message>
        <source>Advanced level</source>
        <translation>Geavanceerd niveau</translation>
    </message>
    <message>
        <source>Suitable skills</source>
        <translation>Geschikte vaardigheden</translation>
    </message>
    <message>
        <source>Very good skills</source>
        <translation>Zeer goede vaardigheden</translation>
    </message>
    <message>
        <source>Perfect skills</source>
        <translation>Perfecte vaardigheden</translation>
    </message>
</context>
<context>
    <name>FingerWidget</name>
    <message>
        <source>Error rates of your fingers</source>
        <translation>Foutpercentages van je vingers</translation>
    </message>
    <message>
        <source>The error rate is based on the recorded characters and the current selected keyboard layout.</source>
        <translation>Het foutenpercentage is gebaseerd op de geregistreerde tekens en de huidige geselecteerde toetsenbordindeling.</translation>
    </message>
    <message>
        <source>Error Rate:</source>
        <translation>Foutpercentage:</translation>
    </message>
    <message>
        <source>Frequency:</source>
        <translation>Frequentie:</translation>
    </message>
    <message>
        <source>Errors:</source>
        <translation>Fouten:</translation>
    </message>
</context>
<context>
    <name>HelpBrowser</name>
    <message>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <source>en</source>
        <translation>nl</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <source>Table of Contents</source>
        <translation>Inhoudsopgave</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>&amp;Sluiten</translation>
    </message>
    <message>
        <source>&amp;Print page</source>
        <translation>&amp;Pagina afdrukken</translation>
    </message>
    <message>
        <source>Print page</source>
        <translation>Pagina afdrukken</translation>
    </message>
</context>
<context>
    <name>IllustrationDialog</name>
    <message>
        <source>Introduction</source>
        <translation>Introductie</translation>
    </message>
    <message>
        <source>Welcome to TIPP10</source>
        <translation>Welkom bij TIPP10</translation>
    </message>
    <message>
        <source>TIPP10 is a free touch typing tutor for Windows, Mac OS and Linux. The ingenious thing about the software is its intelligence feature. Characters that are mistyped are repeated more frequently. Touch typing has never been so easy to learn.</source>
        <translation>TIPP10 is een gratis blindtypeleraar voor Windows, Mac OS en Linux. Het ingenieuze aan de software is de intelligentiefunctie. Verkeerd getypte karakters worden vaker herhaald. Blind typen was nog nooit zo gemakkelijk te leren.</translation>
    </message>
    <message>
        <source>Tips for using the 10 finger system</source>
        <translation>Tips voor het gebruik van het 10-vingersysteem</translation>
    </message>
    <message>
        <source>1. First place your fingers in the home position (this is displayed at the beginning of each lesson). The fingers return to the home row after each key is pressed.</source>
        <translation>1. Plaats eerst je vingers in de beginpositie (deze wordt weergegeven aan het begin van elke les). De vingers keren terug naar de thuisrij nadat elke toets is ingedrukt.</translation>
    </message>
    <message>
        <source>en</source>
        <translation>nl</translation>
    </message>
    <message>
        <source>2. Make sure your posture is straight and avoid looking at the keyboard. Your eyes should be directed toward the monitor at all times.</source>
        <translation>2. Zorg ervoor dat je houding recht is en kijk niet naar het toetsenbord. Je ogen moeten altijd naar de monitor gericht zijn.</translation>
    </message>
    <message>
        <source>3. Bring your arms to the side of your body and relax your shoulders. Your upper arm and lower arm should be at a right angle. Do not rest your wrists and remain in an upright position.</source>
        <translation>3. Breng je armen naar de zijkant van uw lichaam en ontspan uw schouders. Je bovenarm en onderarm moeten in een rechte hoek staan. Laat je polsen niet rusten en blijf rechtop.</translation>
    </message>
    <message>
        <source>4. Try to remain relaxed during the typing lessons.</source>
        <translation>4. Probeer ontspannen te blijven tijdens de typelessen.</translation>
    </message>
    <message>
        <source>5. Try to keep typing errors to a minimum. It is much less efficient to type fast if you are making a lot of mistakes.</source>
        <translation>5. Probeer typefouten tot een minimum te beperken. Het is veel minder efficiënt om snel te typen als je veel fouten maakt.</translation>
    </message>
    <message>
        <source>6. Once you have begun touch typing you have to avoid reverting back to the way you used to type (even if you are in a hurry).</source>
        <translation>6. Als je eenmaal bent begonnen met blind typen, moet je voorkomen dat je terugvalt naar de manier waarop je vroeger typte (zelfs als je haast heeft).</translation>
    </message>
    <message>
        <source>If you need assistance with using the software use the help function.</source>
        <translation>Als je hulp nodig hebt bij het gebruik van de software, gebruik dan de helpfunctie.</translation>
    </message>
    <message>
        <source>All rights reserved.</source>
        <translation>Alle rechten voorbehouden.</translation>
    </message>
    <message>
        <source>&amp;Launch TIPP10</source>
        <translation>&amp;Start TIPP10</translation>
    </message>
    <message>
        <source>Do&amp;n&apos;t show me this window again</source>
        <translation>Laat me dit venster niet meer zie&amp;n</translation>
    </message>
</context>
<context>
    <name>LessonDialog</name>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;Opslaan</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>Name of the lesson (20 characters max.):</source>
        <translation>Naam van de les (max. 20 tekens):</translation>
    </message>
    <message>
        <source>Short description (120 characters max.):</source>
        <translation>Korte beschrijving (max. 120 tekens):</translation>
    </message>
    <message>
        <source>Lesson content (at least two lines):</source>
        <translation>Lesinhoud (minstens twee regels):</translation>
    </message>
    <message>
        <source>&lt;u&gt;Explanation:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Every line (separated by Enter key) is equivalent to a unit of the lesson. There are two types of lesson dictation:&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Sentence Lesson&lt;/b&gt; - every line (sentence) will be dictated exactly how it was entered here with a line break at the end.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Word Lesson&lt;/b&gt; - the lines will be separated by blanks and a line break passes auomatically after at least %1 characters.</source>
        <translation>&lt;u&gt;Uitleg:&lt;/u&gt;&lt;br&gt;&amp;nbsp;&lt;br&gt;Elke regel (gescheiden door Enter-toets) komt overeen met een onderdeel van de les. Er zijn twee soorten lesdictaten:&lt;br&gt; &amp;nbsp;&lt;br&gt;&lt;b&gt;Zinles&lt;/b&gt; - elke regel (zin) wordt precies gedicteerd zoals deze hier is ingevoerd met een regeleinde aan het einde.&lt;br&gt;&amp;nbsp;&lt;br&gt;&lt;b&gt;Woordles &lt;/b&gt; - de regels worden gescheiden door spaties en een regeleinde gaat automatisch voorbij na minstens %1 karakters.</translation>
    </message>
    <message>
        <source>&lt;b&gt;What happens when &quot;Intelligence&quot; is enabled?&lt;/b&gt;&lt;br&gt;With enabled intelligence, the lines to be dictated will be selected depending on the typing mistake quotas instead of dictating them in the right order. Enabling the &quot;Intelligence&quot; only makes sense if the lesson consists of many lines (often &quot;Word Lessons&quot;).&lt;br&gt;</source>
        <translation>&lt;b&gt;Wat gebeurt er als &quot;Intelligentie&quot; is ingeschakeld?&lt;/b&gt;&lt;br&gt;Met ingeschakelde intelligentie worden de te dicteren regels geselecteerd afhankelijk van de typefoutquota in plaats van ze te dicteren in de juiste volgorde. Het inschakelen van de &quot;Intelligentie&quot; heeft alleen zin als de les uit veel regels bestaat (vaak &quot;Woordlessen&quot;).&lt;br&gt;</translation>
    </message>
    <message>
        <source>Dictate the text as:</source>
        <translation>Dicteer de tekst als:</translation>
    </message>
    <message>
        <source>Sentence Lesson</source>
        <translation>Zinles</translation>
    </message>
    <message>
        <source>Every line will be completed with
a line break at the end</source>
        <translation>Elke regel wordt voltooid met
een regeleinde aan het eind</translation>
    </message>
    <message>
        <source>Word Lesson</source>
        <translation>Woordles</translation>
    </message>
    <message>
        <source>Every unit (line) will be separated
by blanks. A line break passes
automatically after at least %1 characters.</source>
        <translation>Elke eenheid (regel) wordt gescheiden
door spaties. Een regeleinde passeert
automatisch na ten minste %1 tekens.</translation>
    </message>
    <message>
        <source>Edit own Lesson</source>
        <translation>Eigen les bewerken</translation>
    </message>
    <message>
        <source>Name of the Lesson:</source>
        <translation>Naam van de les:</translation>
    </message>
    <message>
        <source>Create own Lesson</source>
        <translation>Eigen les maken</translation>
    </message>
    <message>
        <source>Please enter the name of the lesson
</source>
        <translation>Voer de naam van de les in
</translation>
    </message>
    <message>
        <source>Please enter entire lesson content
</source>
        <translation>Voer de volledige lesinhoud in
</translation>
    </message>
    <message>
        <source>Please enter at least two lines of text
</source>
        <translation>Voer ten minste twee regels tekst in
</translation>
    </message>
    <message>
        <source>Please enter 400 lines of text maximum
</source>
        <translation>Voer maximaal 400 regels tekst in
</translation>
    </message>
    <message>
        <source>The name of the lesson already exists. Please enter a new lesson name.
</source>
        <translation>De naam van de les bestaat al. Voer een nieuwe lesnaam in.
</translation>
    </message>
</context>
<context>
    <name>LessonPrintDialog</name>
    <message>
        <source>Print Lesson</source>
        <translation>Les afdrukken</translation>
    </message>
    <message>
        <source>&amp;Print</source>
        <translation>Af&amp;drukken</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <source>Please enter your name:</source>
        <translation>Voer uw naam in:</translation>
    </message>
</context>
<context>
    <name>LessonResult</name>
    <message>
        <source>Print</source>
        <translation>Afdrukken</translation>
    </message>
    <message numerus="yes">
        <source>%n minute(s)</source>
        <translation>
            <numerusform>%n minuut</numerusform>
            <numerusform>%n minuten</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%n character(s)</source>
        <translation>
            <numerusform>%n letter</numerusform>
            <numerusform>%n letters</numerusform>
        </translation>
    </message>
    <message>
        <source>Entire Lesson</source>
        <translation>Hele les</translation>
    </message>
    <message>
        <source>Error Correction with Backspace</source>
        <translation>Foutcorrectie met Backspace</translation>
    </message>
    <message>
        <source>Error Correction without Backspace</source>
        <translation>Foutcorrectie zonder Backspace</translation>
    </message>
    <message>
        <source>Ignore Errors</source>
        <translation>Fouten negeren</translation>
    </message>
    <message>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Alles</translation>
    </message>
    <message>
        <source>- Colored Keys</source>
        <translation>- Gekleurde toetsen</translation>
    </message>
    <message>
        <source>- Home Row</source>
        <translation>- Thuisrij</translation>
    </message>
    <message>
        <source>- Motion Paths</source>
        <translation>- Bewegingspaden</translation>
    </message>
    <message>
        <source>- Separation Line</source>
        <translation>- Scheidingslijn</translation>
    </message>
    <message>
        <source>- Instructions</source>
        <translation>- Instructies</translation>
    </message>
    <message>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message numerus="yes">
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n punt</numerusform>
            <numerusform>%n punten</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>You have reached %1 at a typing speed of %2 cpm and %n typing error(s).</source>
        <translation>
            <numerusform>Je hebt %1 bereikt met een typsnelheid van %2 tpm en %n fouten.</numerusform>
            <numerusform>Je hebt %1 bereikt met een typsnelheid van %2 tpm en %n fouten.</numerusform>
        </translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>Duration: </source>
        <translation>Duur: </translation>
    </message>
    <message>
        <source>Typing Errors: </source>
        <translation>Typefouten: </translation>
    </message>
    <message>
        <source>Assistance: </source>
        <translation>Hulp: </translation>
    </message>
    <message>
        <source>Results</source>
        <translation>Resultaten</translation>
    </message>
    <message>
        <source>Lesson: </source>
        <translation>Les: </translation>
    </message>
    <message>
        <source>Time: </source>
        <translation>Tijd: </translation>
    </message>
    <message>
        <source>Characters: </source>
        <translation>Tekens: </translation>
    </message>
    <message>
        <source>Errors: </source>
        <translation>Fouten: </translation>
    </message>
    <message>
        <source>Error Rate: </source>
        <translation>Foutpercentage: </translation>
    </message>
    <message>
        <source>Cpm: </source>
        <translation>TPM: </translation>
    </message>
    <message>
        <source>Dictation</source>
        <translation>Dicteren</translation>
    </message>
    <message>
        <source>TIPP10 Touch Typing Tutor</source>
        <translation>TIPP10 Tutor blind typen</translation>
    </message>
    <message>
        <source>Report</source>
        <translation>Rapporteren</translation>
    </message>
    <message>
        <source> of %1</source>
        <translation> van %1</translation>
    </message>
    <message>
        <source>Print Report</source>
        <translation>Rapport afdrukken</translation>
    </message>
</context>
<context>
    <name>LessonSqlModel</name>
    <message>
        <source>%L1 s</source>
        <translation>%L1 s</translation>
    </message>
    <message>
        <source>%L1 min</source>
        <translation>%L1 min</translation>
    </message>
    <message>
        <source>%L1 %</source>
        <translation>%L1 %</translation>
    </message>
    <message numerus="yes">
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n punt</numerusform>
            <numerusform>%n punten</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>LessonTableSql</name>
    <message>
        <source>Show: </source>
        <translation>Tonen: </translation>
    </message>
    <message>
        <source>All Lessons</source>
        <translation>Alle lessen</translation>
    </message>
    <message>
        <source>Training Lessons</source>
        <translation>Trainingslessen</translation>
    </message>
    <message>
        <source>Open Lessons</source>
        <translation>Open lessen</translation>
    </message>
    <message>
        <source>Own Lessons</source>
        <translation>Eigen lessen</translation>
    </message>
    <message>
        <source>Lesson</source>
        <translation>Les</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Duur</translation>
    </message>
    <message>
        <source>Characters</source>
        <translation>Tekens</translation>
    </message>
    <message>
        <source>Errors</source>
        <translation>Fouten</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation>Tarief</translation>
    </message>
    <message>
        <source>Cpm</source>
        <translation>Tpm</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>Score</translation>
    </message>
    <message>
        <source>This column shows the names
of completed lessons</source>
        <translation>Deze kolom toont de namen
van afgeronde lessen</translation>
    </message>
    <message>
        <source>Start time of the lesson</source>
        <translation>Starttijd van de les</translation>
    </message>
    <message>
        <source>Total duration of the lesson</source>
        <translation>Totale duur van de les</translation>
    </message>
    <message>
        <source>Number of characters dictated</source>
        <translation>Aantal gedicteerde tekens</translation>
    </message>
    <message>
        <source>Number of typing errors</source>
        <translation>Aantal typefouten</translation>
    </message>
    <message>
        <source>The error rate is calculated as follows:
Errors / Characters
The lower the error rate the better!</source>
        <translation>Het foutenpercentage wordt als volgt berekend:
Fouten / tekens
Hoe lager het foutenpercentage, hoe beter!</translation>
    </message>
    <message>
        <source>&quot;Cpm&quot; indicates how many characters per minute
were entered on average</source>
        <translation>&quot;Tpm&quot; geeft aan hoeveel tekens er gemiddeld per minuut
 werden ingevoerd</translation>
    </message>
    <message>
        <source>The score is calculated as follows:
((Characters - (20 x Errors)) / Duration in minutes) x 0.4

Note that slow typing without errors results in a
better ranking, than fast typing with several errors!</source>
        <translation>De score wordt als volgt berekend:
((Tekens - (20 x Fouten)) / Duur in minuten) x 0,4

Merk op dat langzaam typen zonder fouten resulteert in een
betere ranking dan snel typen met verschillende fouten!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>All results of the current lesson will be discarded!

Do you really want to exit?

</source>
        <translation>Alle resultaten van de huidige les worden verwijderd!

Wilt u echt afsluiten?

</translation>
    </message>
    <message>
        <source>&amp;Go</source>
        <translation>&amp;Gaan</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;Bestand</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation>&amp;Instellingen</translation>
    </message>
    <message>
        <source>E&amp;xit</source>
        <translation>&amp;Verlaten</translation>
    </message>
    <message>
        <source>&amp;Results</source>
        <translation>&amp;Resultaten</translation>
    </message>
    <message>
        <source>&amp;Manual</source>
        <translation>Hand&amp;matig</translation>
    </message>
    <message>
        <source> on the web</source>
        <translation> op het internet</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>&amp;About </source>
        <translation>&amp;Over </translation>
    </message>
    <message>
        <source>ABC-Game</source>
        <translation>ABC-spel</translation>
    </message>
    <message>
        <source>Software Version </source>
        <translation>Softwareversie </translation>
    </message>
    <message>
        <source>Database Version </source>
        <translation>Databaseversie </translation>
    </message>
    <message>
        <source>Portable Version</source>
        <translation>Draagbare versie</translation>
    </message>
    <message>
        <source>Intelligent Touch Typing Tutor</source>
        <translation>Intelligent Typen-leraar</translation>
    </message>
    <message>
        <source>On the web: </source>
        <translation>Op het internet: </translation>
    </message>
    <message>
        <source>TIPP10 is published under the GNU General Public License and is available for free. You do not have to pay for it wherever you download it!</source>
        <translation>TIPP10 is gepubliceerd onder de GNU General Public License en is gratis beschikbaar. U hoeft er niet voor te betalen, waar u het ook downloadt!</translation>
    </message>
    <message>
        <source>About </source>
        <translation>Over </translation>
    </message>
</context>
<context>
    <name>ProgressionWidget</name>
    <message>
        <source>Time</source>
        <translation>Tijd</translation>
    </message>
    <message>
        <source>Show: </source>
        <translation>Tonen: </translation>
    </message>
    <message>
        <source>All Lessons</source>
        <translation>Alle lessen</translation>
    </message>
    <message>
        <source>Training Lessons</source>
        <translation>Trainingslessen</translation>
    </message>
    <message>
        <source>Open Lessons</source>
        <translation>Open lessen</translation>
    </message>
    <message>
        <source>Own Lessons</source>
        <translation>Eigen lessen</translation>
    </message>
    <message>
        <source>Order by x-axis:</source>
        <translation>Sorteer op x-as:</translation>
    </message>
    <message>
        <source>Lesson</source>
        <translation>Les</translation>
    </message>
    <message>
        <source>Points</source>
        <translation>Punten</translation>
    </message>
    <message>
        <source>Training Lesson</source>
        <translation>Trainingsles</translation>
    </message>
    <message>
        <source>Open Lesson</source>
        <translation>Open les</translation>
    </message>
    <message>
        <source>Own Lesson</source>
        <translation>Eigen les</translation>
    </message>
    <message numerus="yes">
        <source>%n point(s)</source>
        <translation>
            <numerusform>%n punt</numerusform>
            <numerusform>%n punten</numerusform>
        </translation>
    </message>
    <message>
        <source>%1 cpm</source>
        <translation>%1 tpm</translation>
    </message>
    <message>
        <source>The progress graph will be shown after completing two lessons.</source>
        <translation>De voortgangsgrafiek wordt getoond na het voltooien van twee lessen.</translation>
    </message>
    <message>
        <source>Cpm</source>
        <translation>Tpm</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>Score</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Left little finger</source>
        <translation>Linker pink</translation>
    </message>
    <message>
        <source>Left ring finger</source>
        <translation>Linker ringvinger</translation>
    </message>
    <message>
        <source>Left middle finger</source>
        <translation>Linker middelvinger</translation>
    </message>
    <message>
        <source>Left forefinger</source>
        <translation>Linker wijsvinger</translation>
    </message>
    <message>
        <source>Right forefinger</source>
        <translation>Rechter wijsvinger</translation>
    </message>
    <message>
        <source>Right middle finger</source>
        <translation>Rechter middelvinger</translation>
    </message>
    <message>
        <source>Right ring finger</source>
        <translation>Rechterringvinger</translation>
    </message>
    <message>
        <source>Right little finger</source>
        <translation>Rechter pink</translation>
    </message>
    <message>
        <source>Thumb</source>
        <translation>Duim</translation>
    </message>
    <message>
        <source>en</source>
        <translation>nl</translation>
    </message>
    <message>
        <source>Affected directory:
</source>
        <translation>Getroffen map:
</translation>
    </message>
</context>
<context>
    <name>RegExpDialog</name>
    <message>
        <source>Filter for the keyboard layout</source>
        <translation>Filter voor de toetsenbordindeling</translation>
    </message>
    <message>
        <source>Limitation of characters</source>
        <translation>Beperking van tekens</translation>
    </message>
    <message>
        <source>You should try to avoid using characters not supported by your keyboard layout. A filter as a regular expression is applied to all practice texts before the lesson begins. You should only make changes here if you are familiar with regular expressions.</source>
        <translation>Je moet proberen tekens te vermijden die niet worden ondersteund door je toetsenbordindeling. Een filter als reguliere expressie wordt toegepast op alle oefenteksten voordat de les begint. Je moet hier alleen wijzigingen aanbrengen als je bekend bent met reguliere expressies.</translation>
    </message>
    <message>
        <source>Replacement Filter</source>
        <translation>Vervangingsfilter</translation>
    </message>
    <message>
        <source>Filtering unauthorized characters can produce texts that make little sense (e.g., by removing umlauts). You can define replacements that will be applied before the limitation of characters is applied. Please follow the example here that replaces all Germans umlauts and the ß symbol:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</source>
        <translation>Het filteren van niet-geautoriseerde tekens kan teksten produceren die weinig zin hebben (bijvoorbeeld door umlauten te verwijderen). U kunt vervangingen definiëren die worden toegepast voordat de beperking van tekens wordt toegepast. Volg hier het voorbeeld dat alle Duitse umlauten vervangt en het ß-symbool:
ae=ae,oe=oe,ue=ue,Ae=Ae,Oe=Oe,Ue=Ue,ss=ss</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>Training</source>
        <translation>Training</translation>
    </message>
    <message>
        <source>Ticker</source>
        <translation>Tikker</translation>
    </message>
    <message>
        <source>Here you can select the background color</source>
        <translation>Hier kunt u de achtergrondkleur selecteren</translation>
    </message>
    <message>
        <source>Here you can select the font color</source>
        <translation>Hier kunt u de letterkleur selecteren</translation>
    </message>
    <message>
        <source>Cursor:</source>
        <translation>Cursor:</translation>
    </message>
    <message>
        <source>Here you can change the font of the ticker (a font size larger than 20 pt is not recommended)</source>
        <translation>Hier kunt u het lettertype van de lichtkrant wijzigen (een lettergrootte groter dan 20 pt wordt niet aanbevolen)</translation>
    </message>
    <message>
        <source>Change &amp;Font</source>
        <translation>&amp;Lettertype wijzigen</translation>
    </message>
    <message>
        <source>Here can select the color of the cursor for the current character</source>
        <translation>Hier kan de kleur van de cursor voor het huidige teken worden geselecteerd</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>Lettertype:</translation>
    </message>
    <message>
        <source>Font Color:</source>
        <translation>Lettertypekleur:</translation>
    </message>
    <message>
        <source>Background:</source>
        <translation>Achtergrond:</translation>
    </message>
    <message>
        <source>Speed:</source>
        <translation>Snelheid:</translation>
    </message>
    <message>
        <source>Off</source>
        <translation>Uit</translation>
    </message>
    <message>
        <source>Here you can change the speed of the ticker (Slider on the left: Ticker does not move until reaching the end of line. Slider on the right: The ticker moves very fast.)</source>
        <translation>Hier kunt u de snelheid van de ticker wijzigen (Slider aan de linkerkant: Ticker beweegt niet totdat het einde van de regel is bereikt. Slider aan de rechterkant: de ticker beweegt erg snel.)</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation>Snel</translation>
    </message>
    <message>
        <source>Audio Output</source>
        <translation>Audio-uitvoer</translation>
    </message>
    <message>
        <source>Here you can activate a metronome</source>
        <translation>Hier kun je een metronoom activeren</translation>
    </message>
    <message>
        <source>Metronome:</source>
        <translation>Metronoom:</translation>
    </message>
    <message>
        <source>Select how often the metronome sound should appear per minute</source>
        <translation>Selecteer hoe vaak het metronoomgeluid per minuut moet verschijnen</translation>
    </message>
    <message>
        <source> cpm</source>
        <translation> tpm</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation>Geavanceerd</translation>
    </message>
    <message>
        <source>Training Lessons:</source>
        <translation>Trainingslessen:</translation>
    </message>
    <message>
        <source>The training lessons you have chosen are not suited for the keyboard layout. You can continue but you may have to put aside some keys from the beginning.</source>
        <translation>De trainingslessen die je hebt gekozen, zijn niet geschikt voor de toetsenbordindeling. Je kunt doorgaan, maar het kan zijn dat je vanaf het begin wat toetsen opzij moet leggen.</translation>
    </message>
    <message>
        <source>Keyboard Layout:</source>
        <translation>Toetsenbordindeling:</translation>
    </message>
    <message>
        <source>Results</source>
        <translation>Resultaten</translation>
    </message>
    <message>
        <source>User Data</source>
        <translation>Gebruikersgegevens</translation>
    </message>
    <message>
        <source>Here you can reset all saved lesson data (the lessons will be empty as they were after initial installation</source>
        <translation>Hier kun je alle opgeslagen lesgegevens resetten (de lessen zijn leeg zoals ze waren na de eerste installatie</translation>
    </message>
    <message>
        <source>Reset &amp;completed lessons</source>
        <translation>Reset &amp;voltooide lessen</translation>
    </message>
    <message>
        <source>Here you can reset all recorded keystrokes and typing mistakes (the characters will be empty as they were after initial installation)</source>
        <translation>Hier kunt u alle geregistreerde toetsaanslagen en typefouten resetten (de tekens zijn leeg zoals ze waren na de eerste installatie)</translation>
    </message>
    <message>
        <source>Reset all &amp;recorded characters</source>
        <translation>Alle opgenomen tekens &amp;resetten</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Overig</translation>
    </message>
    <message>
        <source>Windows</source>
        <translation>Vensters</translation>
    </message>
    <message>
        <source>Here you can decide if an information window with tips is shown at the beginning</source>
        <translation>Hier kunt u bepalen of er aan het begin een informatievenster met tips wordt getoond</translation>
    </message>
    <message>
        <source>Show welcome message at startup</source>
        <translation>Toon welkomstbericht bij opstarten</translation>
    </message>
    <message>
        <source>Here you can define whether a warning window is displayed when an open or own lesson with active intelligence is started</source>
        <translation>Hier kunt u definiëren of er een waarschuwingsvenster wordt weergegeven wanneer een open of eigen les met actieve intelligentie wordt gestart</translation>
    </message>
    <message>
        <source>Remember enabled intelligence before starting
an open or own lesson</source>
        <translation>Onthoud ingeschakelde intelligentie voor het starten
van een open of eigen les</translation>
    </message>
    <message>
        <source>Change duration of the lesson automatically to
&quot;Entire lesson&quot; when disabling the intelligence</source>
        <translation>Wijzig de duur van de les automatisch in
&quot;Hele les&quot; bij het uitschakelen van de intelligentie</translation>
    </message>
    <message>
        <source>Some of your settings require a restart of the software to take effect.
</source>
        <translation>Sommige van uw instellingen vereisen een herstart van de software om van kracht te worden.
</translation>
    </message>
    <message>
        <source>All data for completed lessons for the current user will be deleted
and the lesson list will return to its original state after initial installation!

Do you really want to continue?

</source>
        <translation>Alle gegevens voor voltooide lessen voor de huidige gebruiker worden verwijderd
en en de lessenlijst keert terug naar de oorspronkelijke staat na de eerste installatie!

Wilt u echt doorgaan?

</translation>
    </message>
    <message>
        <source>The data for completed lessons was successfully deleted!
</source>
        <translation>De gegevens voor voltooide lessen zijn succesvol verwijderd!
</translation>
    </message>
    <message>
        <source>All recorded characters (mistake quotas) of the current user will be deleted and the character list will return to its original state after initial installation!

Do you really want to continue?</source>
        <translation>Alle geregistreerde tekens (foutquota) van de huidige gebruiker worden verwijderd en de lijst met tekens keert terug naar de oorspronkelijke staat na de eerste installatie!

Wilt u echt doorgaan?</translation>
    </message>
    <message>
        <source>The recorded characters were successfully deleted!
</source>
        <translation>De opgenomen tekens zijn succesvol verwijderd!
</translation>
    </message>
</context>
<context>
    <name>StartWidget</name>
    <message>
        <source>Training Lessons</source>
        <translation>Trainingslessen</translation>
    </message>
    <message>
        <source>Subject:</source>
        <translation>Onderwerp:</translation>
    </message>
    <message>
        <source>&amp;Edit</source>
        <translation>&amp;Bewerken</translation>
    </message>
    <message>
        <source>&amp;New Lesson</source>
        <translation>&amp;Nieuwe les</translation>
    </message>
    <message>
        <source>&amp;Import Lesson</source>
        <translation>Les &amp;importeren</translation>
    </message>
    <message>
        <source>&amp;Export Lesson</source>
        <translation>&amp;Exporteer les</translation>
    </message>
    <message>
        <source>&amp;Edit Lesson</source>
        <translation>Les be&amp;werken</translation>
    </message>
    <message>
        <source>&amp;Delete Lesson</source>
        <translation>&amp;Verwijder les</translation>
    </message>
    <message>
        <source>Duration of Lesson</source>
        <translation>Duur van de les</translation>
    </message>
    <message>
        <source>Time Limit:</source>
        <translation>Tijdslimiet:</translation>
    </message>
    <message>
        <source>The dictation will be stopped after
a specified time period</source>
        <translation>Het dicteren wordt gestopt na
een opgegeven tijdsperiode</translation>
    </message>
    <message>
        <source> minutes</source>
        <translation> minuten</translation>
    </message>
    <message>
        <source>Character Limit:</source>
        <translation>Tekenlimiet:</translation>
    </message>
    <message>
        <source>The dictation will be stopped after
a specified number of correctly typed
characters</source>
        <translation>Het dicteren wordt gestopt na
een bepaald aantal correct getypte
tekens</translation>
    </message>
    <message>
        <source> characters</source>
        <translation> tekens</translation>
    </message>
    <message>
        <source>Entire
Lesson</source>
        <translation>Gehele
Les</translation>
    </message>
    <message>
        <source>The complete lesson will be dictated
from beginning to end</source>
        <translation>De volledige les wordt gedicteerd
van begin tot eind</translation>
    </message>
    <message>
        <source>(Entire Lesson)</source>
        <translation>(hele les)</translation>
    </message>
    <message>
        <source>Response to Typing Errors</source>
        <translation>Reactie op typefouten</translation>
    </message>
    <message>
        <source>Block Typing Errors</source>
        <translation>Typefouten blokkeren</translation>
    </message>
    <message>
        <source>The dictation will only proceed if the correct
key was pressed</source>
        <translation>Het dicteren gaat alleen verder als de juiste
toets is ingedrukt</translation>
    </message>
    <message>
        <source>Correction with Backspace</source>
        <translation>Correctie met Backspace</translation>
    </message>
    <message>
        <source>Typing errors have to be removed
with the return key</source>
        <translation>Typefouten moeten worden verwijderd
met de enter-toets</translation>
    </message>
    <message>
        <source>Audible Signal</source>
        <translation>Akoestisch signaal</translation>
    </message>
    <message>
        <source>A beep sounds with every typing error</source>
        <translation>Bij elke typefout klinkt een piep</translation>
    </message>
    <message>
        <source>Show key picture</source>
        <translation>Toon hoofdfoto</translation>
    </message>
    <message>
        <source>For every typing error the corresponding key picture is displayed on the keyboard</source>
        <translation>Voor elke typefout wordt de bijbehorende sleutelafbeelding weergegeven op het toetsenbord</translation>
    </message>
    <message>
        <source>*The text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*De tekst van de les wordt niet in de bedoelde volgorde gedicteerd, maar wordt in realtime aangepast aan uw typefouten.</translation>
    </message>
    <message>
        <source>*Select this option if the text of the lesson will not be dictated in its intended sequence, but will be adjusted in real time to your typing errors.</source>
        <translation>*Selecteer deze optie als de tekst van de les niet in de bedoelde volgorde wordt gedicteerd, maar in realtime wordt aangepast aan uw typefouten.</translation>
    </message>
    <message>
        <source>Intelligence</source>
        <translation>Intelligentie</translation>
    </message>
    <message>
        <source>Based on the current error rates of all characters, the wordsand phrases of the dictation will be selected in real time.On the other hand, if the intelligence box is not checked, the text ofthe lesson is always dictated in the same order.</source>
        <translation>Op basis van de huidige foutpercentages van alle karakters, zullen de woorden en zinnen van het dictaat in realtime worden geselecteerd. Aan de andere kant, als het intelligentievak niet is aangevinkt, wordt de tekst van de les altijd gedicteerd in de dezelfde bestelling.</translation>
    </message>
    <message>
        <source>Assistance</source>
        <translation>Hulp</translation>
    </message>
    <message>
        <source>Show Keyboard</source>
        <translation>Toon toetsenbord</translation>
    </message>
    <message>
        <source>For visual support, the virtual keyboard and
status information is shown</source>
        <translation>Voor visuele ondersteuning wordt het virtuele toetsenbord en
statusinformatie getoond</translation>
    </message>
    <message>
        <source>Colored Keys</source>
        <translation>Gekleurde toetsen</translation>
    </message>
    <message>
        <source>For visual support pressing keys will be
marked with colors</source>
        <translation>Voor visuele ondersteuning wordt het indrukken van toetsen gemarkeerd met kleuren</translation>
    </message>
    <message>
        <source>Home Row</source>
        <translation>Thuisrij</translation>
    </message>
    <message>
        <source>For visual support, the remaining fingers
of the home row will be colored</source>
        <translation>Voor visuele ondersteuning zullen de resterende vingers
van de thuisrij gekleurd zijn</translation>
    </message>
    <message>
        <source>L/R Separation Line</source>
        <translation>L/R scheidingslijn</translation>
    </message>
    <message>
        <source>For visual support a separation line between left
and right hand will be shown</source>
        <translation>Ter visuele ondersteuning wordt een scheidingslijn tussen de linker
en rechterhand getoond</translation>
    </message>
    <message>
        <source>Instruction</source>
        <translation>Instructie</translation>
    </message>
    <message>
        <source>Show fingers to be used in the status bar</source>
        <translation>Toon te gebruiken vingers in de statusbalk</translation>
    </message>
    <message>
        <source>Motion Paths</source>
        <translation>Bewegingspaden</translation>
    </message>
    <message>
        <source>Motion paths of the fingers will be shown
on the keyboard</source>
        <translation>Bewegingspaden van de vingers worden getoond
iet op het toetsenbord</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>&amp;Start Training</source>
        <translation>&amp;Begin met trainen</translation>
    </message>
    <message>
        <source>All</source>
        <translation>Alles</translation>
    </message>
    <message>
        <source>Open Lessons</source>
        <translation>Open lessen</translation>
    </message>
    <message>
        <source>At the moment open lessons only exists in German language. We hope to provide open lessons in English soon.</source>
        <translation>Op dit moment bestaan er alleen open lessen in de Duitse taal. We hopen binnenkort open lessen in het Engels aan te bieden.</translation>
    </message>
    <message>
        <source>Own Lessons</source>
        <translation>Eigen lessen</translation>
    </message>
    <message>
        <source>Please select a text file</source>
        <translation>Selecteer een tekstbestand</translation>
    </message>
    <message>
        <source>Text files (*.txt)</source>
        <translation>Tekstbestanden (*.txt)</translation>
    </message>
    <message>
        <source>Please indicate the location of a text file</source>
        <translation>Geef de locatie van een tekstbestand aan</translation>
    </message>
    <message>
        <source>Do you really want to delete the lesson, and all the recorded data in the context of this lesson?</source>
        <translation>Wilt u de les echt verwijderen, en alle opgenomen gegevens in de context van deze les?</translation>
    </message>
</context>
<context>
    <name>TickerBoard</name>
    <message>
        <source>Dictation Finished</source>
        <translation>Dictee voltooid</translation>
    </message>
    <message>
        <source>Press space bar to proceed</source>
        <translation>Druk op de spatiebalk om verder te gaan</translation>
    </message>
</context>
<context>
    <name>TrainingWidget</name>
    <message>
        <source>&amp;Pause</source>
        <translation>&amp;Pauze</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&amp;Annuleren</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Help</translation>
    </message>
    <message>
        <source>Press space bar to start</source>
        <translation>Druk op de spatiebalk om te starten</translation>
    </message>
    <message>
        <source>Take Home Row position</source>
        <translation>Positie thuisrij nemen</translation>
    </message>
    <message>
        <source>Do you really want to exit the lesson early?</source>
        <translation>Wil je de les echt eerder verlaten?</translation>
    </message>
    <message>
        <source>Do you want to save your results?</source>
        <translation>Wilt u uw resultaten opslaan?</translation>
    </message>
    <message>
        <source>E&amp;xit Lesson early</source>
        <translation>Les vroegtijdig &amp;verlaten</translation>
    </message>
    <message>
        <source>Errors: </source>
        <translation>Fouten: </translation>
    </message>
    <message>
        <source>Cpm: </source>
        <translation>TPM: </translation>
    </message>
    <message>
        <source>Time: </source>
        <translation>Tijd: </translation>
    </message>
    <message>
        <source>Characters: </source>
        <translation>Tekens: </translation>
    </message>
</context>
</TS>
