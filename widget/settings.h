/*
Copyright (c) 2006-2009, Tom Thielicke IT Solutions
Copyright (c) 2021, André Apitzsch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include <QFont>

namespace Ui {
class Settings;
}

class Settings : public QDialog {
    Q_OBJECT

public:
    explicit Settings(QWidget* parent = nullptr);
    ~Settings();

private slots:
    void on_buttonFont_clicked();
    void on_buttonFontColor_clicked();
    void on_buttonBackground_clicked();
    void on_buttonCursor_clicked();
    void on_buttonLayoutAdvanced_clicked();
    void on_buttonResetLessons_clicked();
    void on_buttonResetRecordedChars_clicked();
    void checkLessonToLayout();
    void clearLayoutSetting();

private:
    void showHelp();
    void save();
    void readSettings();
    bool writeSettings();
    void setFontButtonLabel();

    Ui::Settings* ui;
    QFont tickerFont;
};

#endif // SETTINGS_H
