/*
Copyright (c) 2006-2011, Tom Thielicke IT Solutions

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
*/

/****************************************************************
**
** File name: defines.h
**
****************************************************************/

#ifndef DEFINES_H
#define DEFINES_H

#include <QString>
#include <cstdint>

namespace t10 {
#if APP_PORTABLE || defined(APP_WIN)
const QString tipp10_datadir
    = QCoreApplication::applicationDirPath() + "/../share/tipp10/";
#else

#ifndef INSTALLPREFIX
#define INSTALLPREFIX "/usr/local"
#endif
// must end with a trailing /
const QString tipp10_datadir = QString(INSTALLPREFIX "/share/tipp10/");
#endif

// Languages
const QString app_std_language_layout = "de_qwertz_win";
const QString app_std_language_lesson = "de_de_qwertz";

// Common program constants
const QString app_name_intern = "TIPP10";
const QString app_name = "TIPP10";
const QString app_url = "https://gitlab.com/tipp10/tipp10";
const QString app_db = "tipp10v2.template";
const QString app_user_db = "tipp10v2.db";
const QString app_version = "3.3.0";

// Update constants
const QString update_url_sql = "/update/sql.tipp10v210.utf";
const int32_t compiled_update_version = 33;

// Lesson text constants
const int32_t num_token_until_refresh = 25;
const int32_t num_token_until_new_line = 35;
const int32_t num_intelligent_queries = 2;
const QChar token_new_line = QChar(0x00b6);
const QChar token_tab = QChar(0x2192);
const QChar token_backspace = QChar(0x00b9); // 0x00ac

// Constants for dynamic training
const int32_t num_text_until_repeat = 10;
const int32_t border_lesson_is_sentence = 7;
const int32_t last_lession = 18; // lesson with training of all characters
const int32_t numpad_lesson_start = 19;
const bool synchron_db_while_training = false;

// Standard constants
const int32_t lesson_timelen_standard = 5;
const int32_t lesson_tokenlen_standard = 500;
const int32_t metronom_standard = 60;
const int32_t tickerspeed_standard = 2;
const QString ticker_color_font = "#000000";
const QString ticker_color_bg = "#FFFFFF";
const QString ticker_color_cursor = "#CDCDCD";

// Font format
const QString font_standard = "Arial";
const int font_size_ticker = 18;
const int font_size_ticker_pause = 16;
const int font_size_status = 8;
const int font_size_progress = 8;
const int font_size_progress_lesson = 6;
const int font_size_finger = 10;

// Window dimensions
const int app_width_standard = 750;
const int app_height_standard = 520;
const int app_width_small = 680;
const int app_height_small = 210;
}

#endif // DEFINES_H
